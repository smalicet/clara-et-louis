Hello,  
  
Some news about Clara & Louis.  
They are very [busy](readme.md) (Clara enjoys doing planing and did it herself ...)  
We are teaching them at home (a lot of Math and French, cycling and computing but unfortunally no English except in books with our poor level to help them) :   

![](./Ressources/magformers.jpg)
![](./Ressources/algorea.jpg)

They also made a dance :(click on the picture to see it)    
[![](http://img.youtube.com/vi/Ft7iZ9xZmL0/0.jpg)](http://www.youtube.com/watch?v=Ft7iZ9xZmL0 "")

They were happy to have some challenges to do and here are the pictures of some of them :  
  
  

## Defi n°1 :  
![](./Ressources/defi1-clara.jpg)
![](./Ressources/defi1-louis.jpg)

## Defi n°2 :  
![](./Ressources/defi2-clara.jpg)
![](./Ressources/defi2-louis.jpg)  

## Defi n°3 :  
![](./Ressources/defi3-clara.jpg)
![](./Ressources/defi3-louis.jpg)
![](./Ressources/defi3.jpg)

## Defi n°4 :  
![](./Ressources/defi4.jpeg)
  

## Defi n°5 :  
![](./Ressources/defi5.jpg)
  
  
## Defi n°6 :  
![](./Ressources/defi6.jpg)
  
  
## Defi n°7 :  
![](./Ressources/defi7-clara1.jpg)
![](./Ressources/defi7-clara2.jpg)
![](./Ressources/defi7-louis.jpg)
  
## Defi n°11 :  
![](./Ressources/defi11-clara1.jpg)
![](./Ressources/defi11-louis1.jpg)
![](./Ressources/defi11-clara2.jpg)
![](./Ressources/defi11-louis2.jpg)
![](./Ressources/defi11.jpg)

## Defi n°14 :  
![](./Ressources/defi14.jpg)
  
  


